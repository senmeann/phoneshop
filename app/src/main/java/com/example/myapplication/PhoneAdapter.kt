package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class PhoneAdapter(
    private val context:MainActivity,
    private val phoneList:ArrayList<Phone>):RecyclerView.Adapter<PhoneAdapter.ViewHolder>(){
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.name?.text = phoneList[position].name
        holder?.price?.text = "$"+phoneList[position].price.toString()
        Picasso.get().load(phoneList[position].imageUrl).into(holder?.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(context).inflate(R.layout.phone_itme,parent,false))
    }

    override fun getItemCount(): Int {
        if(phoneList != null){
            return phoneList.size
        }
        return  0
    }

    class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        val name = view.findViewById<TextView>(R.id.phone_name)
        val price = view.findViewById<TextView>(R.id.phone_price)
        val image = view.findViewById<ImageView>(R.id.phone_picture)
    }
}