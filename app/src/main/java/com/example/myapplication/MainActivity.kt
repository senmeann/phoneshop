package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray



class MainActivity : AppCompatActivity() {
    private val phoneList = ArrayList<Phone>();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar.setTitle("Phone Shop")

        val layoutManager = LinearLayoutManager(this)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = layoutManager
        val queue = Volley.newRequestQueue(this)
        val url = "http://lp.js-cambodia.com/rupp/phones.php"
        val  jsonArrayRequest = JsonArrayRequest(
            url,
            Response.Listener<JSONArray> {
                    response ->
                for(i in 0 until (response.length() )){
                    val jsonObject = response.getJSONObject(i)
                    val name:String = jsonObject.getString("name")
                    val id:Int = jsonObject.getInt("id")
                    val price = jsonObject.getInt("price")
                    val imageUrl:String = jsonObject.getString("imageUrl")
                    val phone = Phone(name,id,imageUrl,price)
                    phoneList.add(phone)
                }
                recyclerView.adapter = PhoneAdapter(this,phoneList)

            },
            Response.ErrorListener{ error ->
                Toast.makeText(this,error.toString(),Toast.LENGTH_LONG).show()
            }
        )
        queue.add(jsonArrayRequest)

        btn_filter.setOnClickListener {
            val intent = Intent(this, FilterActivity::class.java)
            startActivity(intent)
        }
    }


}
