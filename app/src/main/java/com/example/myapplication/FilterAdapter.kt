package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class FilterAdapter(
    private val context:FilterActivity,
    private val filterList:ArrayList<Filter>):RecyclerView.Adapter<FilterAdapter.ViewHolder>(){
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.name?.text = filterList[position].name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(context).inflate(R.layout.filter_items,parent,false))
    }

    override fun getItemCount(): Int {
        if(filterList != null){
            return filterList.size
        }
        return  0
    }

    class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        val name = view.findViewById<TextView>(R.id.filter)
    }
}