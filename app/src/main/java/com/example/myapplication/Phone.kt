package com.example.myapplication
data class  Phone(
    val name:String,
    val id:Int,
    val imageUrl:String,
    val price:Int){
}
