package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_filter.*
import org.json.JSONArray

class FilterActivity : AppCompatActivity() {
    private val filterList = ArrayList<Filter>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        var toolbar = findViewById<Toolbar>(R.id.toolbar).also {
            it.setTitle(R.string.app_toolbar_name)
            setSupportActionBar(it)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { it: View? ->
            onBackPressed()
        }
        onSupportNavigateUp()
        filter_rcv.layoutManager = LinearLayoutManager(this)
        val queue = Volley.newRequestQueue(this)
        val url = "http://lp.js-cambodia.com/rupp/brands.php"
        val  jsonArrayRequest = JsonArrayRequest(
            url,
            Response.Listener<JSONArray> {
                    response ->
                for(i in 0 until (response.length() )){
                    val jsonObject = response.getJSONObject(i)
                    val name:String = jsonObject.getString("name")
                    val id:Int = jsonObject.getInt("id")
                    val filter = Filter(name,id)
                    filterList.add(filter)
                }
                filter_rcv.adapter = FilterAdapter( this, filterList)

            },
            Response.ErrorListener{ error ->
                Toast.makeText(this,error.toString(), Toast.LENGTH_LONG).show()
            }
        )
        queue.add(jsonArrayRequest)
    }
}
